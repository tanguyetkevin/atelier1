// screen.cc

#include<iostream>
#include<cstdlib>
#include<string>
#include "SDL.h"
#include "SDL_image.h"
using namespace std;

int
main(int argc, char* argv[])
{
    int temps = 0;
    bool quit=false;

    SDL_Surface *screen;
    SDL_Event event;

    SDL_Rect square;

    {
        square.x = 640/2-10;
        square.y = 480/2-10;
        square.w = 20;
        square.h = 20;
    }

    SDL_Init(SDL_INIT_EVERYTHING);
    screen=SDL_SetVideoMode(640,480,32,SDL_SWSURFACE);

    while(!quit)
    {

        switch (temps < 1500)
        {
        case 1:
            temps += 10;
            SDL_FillRect(screen,&screen->clip_rect,
                         SDL_MapRGB(screen->format,255,255,255));

            SDL_Flip(screen);
            break;
        case 0:

            SDL_FillRect(screen,& square, SDL_MapRGB(screen->format,255,0,0));

            SDL_Flip(screen);
            temps += 10;
            break;
        }
        SDL_Delay(10);
        if (temps >= 3000)
            temps = 0;

        while(SDL_PollEvent(&event))
            if(event.type==SDL_QUIT)
                quit=true;
    }

    SDL_FreeSurface(screen);
    SDL_Quit();
    return EXIT_SUCCESS;
}
